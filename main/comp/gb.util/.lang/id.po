#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.util 3.16.90\n"
"POT-Creation-Date: 2022-02-04 01:26 UTC\n"
"PO-Revision-Date: 2022-02-04 01:30 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Utility routines"
msgstr ""

#: File.class:22
msgid "&1 B"
msgstr ""

#: File.class:24
msgid "&1 KiB"
msgstr ""

#: File.class:26
msgid "&1 MiB"
msgstr ""

#: File.class:28
msgid "&1 GiB"
msgstr ""

#: File.class:34
msgid "&1 KB"
msgstr ""

#: File.class:36
msgid "&1 MB"
msgstr ""

#: File.class:38
msgid "&1 GB"
msgstr ""

#: Language.class:14
msgid "Afrikaans (South Africa)"
msgstr ""

#: Language.class:17
msgid "Arabic (Egypt)"
msgstr "Arab (Mesir)"

#: Language.class:18
msgid "Arabic (Tunisia)"
msgstr "Arab (Tunisia)"

#: Language.class:21
msgid "Azerbaijani (Azerbaijan)"
msgstr ""

#: Language.class:24
msgid "Bulgarian (Bulgaria)"
msgstr "Bulgaria (Bulgaria)"

#: Language.class:27
msgid "Catalan (Catalonia, Spain)"
msgstr "Catalan (Catalonia, Spain)"

# gb-ignore
#: Language.class:31
msgid "Welsh (United Kingdom)"
msgstr ""

#: Language.class:34
msgid "Czech (Czech Republic)"
msgstr ""

#: Language.class:37
msgid "Danish (Denmark)"
msgstr "Danish (Denmark)"

#: Language.class:40
msgid "German (Germany)"
msgstr "German (Germany)"

#: Language.class:41
msgid "German (Belgium)"
msgstr "German (Belgium)"

#: Language.class:44
msgid "Greek (Greece)"
msgstr "Greek (Greece)"

#: Language.class:47
msgid "English (common)"
msgstr "Inggris (umum)"

#: Language.class:48
msgid "English (United Kingdom)"
msgstr "Inggris (United Kingdom)"

#: Language.class:49
msgid "English (U.S.A.)"
msgstr "Inggris (U.S.A.)"

#: Language.class:50
msgid "English (Australia)"
msgstr "Inggris (Australia)"

#: Language.class:51
msgid "English (Canada)"
msgstr "Inggris (Canada)"

#: Language.class:54
msgid "Esperanto (Anywhere!)"
msgstr ""

#: Language.class:57
msgid "Spanish (common)"
msgstr ""

#: Language.class:58
msgid "Spanish (Spain)"
msgstr "Spanish (Spain)"

#: Language.class:59
msgid "Spanish (Argentina)"
msgstr "Spanish (Argentina)"

#: Language.class:62
msgid "Estonian (Estonia)"
msgstr ""

#: Language.class:65
msgid "Basque (Basque country)"
msgstr ""

#: Language.class:68
msgid "Farsi (Iran)"
msgstr ""

#: Language.class:71
msgid "Finnish (Finland)"
msgstr ""

#: Language.class:74
msgid "French (France)"
msgstr "French (France)"

#: Language.class:75
msgid "French (Belgium)"
msgstr "French (Belgium)"

#: Language.class:76
msgid "French (Canada)"
msgstr "French (Canada)"

#: Language.class:77
msgid "French (Switzerland)"
msgstr "French (Switzerland)"

#: Language.class:80
msgid "Galician (Spain)"
msgstr "Galicia (Spanyol)"

#: Language.class:83
msgid "Hebrew (Israel)"
msgstr ""

#: Language.class:86
msgid "Hindi (India)"
msgstr ""

#: Language.class:89
msgid "Hungarian (Hungary)"
msgstr "Hungarian (Hungary)"

#: Language.class:92
msgid "Croatian (Croatia)"
msgstr "Kroasia (Kroasia)"

#: Language.class:95
msgid "Indonesian (Indonesia)"
msgstr "Indonesian (Indonesia)"

#: Language.class:98
msgid "Irish (Ireland)"
msgstr "Irish (Irlandia)"

#: Language.class:101
msgid "Icelandic (Iceland)"
msgstr ""

#: Language.class:104
msgid "Italian (Italy)"
msgstr "Italian (Itali)"

#: Language.class:107
msgid "Japanese (Japan)"
msgstr ""

#: Language.class:110
msgid "Khmer (Cambodia)"
msgstr ""

#: Language.class:113
msgid "Korean (Korea)"
msgstr ""

#: Language.class:116
msgid "Latin"
msgstr ""

#: Language.class:119
msgid "Lithuanian (Lithuania)"
msgstr ""

#: Language.class:122
msgid "Malayalam (India)"
msgstr ""

#: Language.class:125
msgid "Macedonian (Republic of Macedonia)"
msgstr ""

#: Language.class:128
msgid "Dutch (Netherlands)"
msgstr "Dutch (Netherlands)"

#: Language.class:129
msgid "Dutch (Belgium)"
msgstr "Dutch (Belgium)"

#: Language.class:132
msgid "Norwegian (Norway)"
msgstr "Norwegian (Norway)"

#: Language.class:135
msgid "Punjabi (India)"
msgstr ""

#: Language.class:138
msgid "Polish (Poland)"
msgstr "Polish (Poland)"

#: Language.class:141
msgid "Portuguese (Portugal)"
msgstr "Portugis (Portugal)"

#: Language.class:142
msgid "Portuguese (Brazil)"
msgstr "Portugis (Brazil)"

#: Language.class:145
msgid "Valencian (Valencian Community, Spain)"
msgstr ""

#: Language.class:148
msgid "Romanian (Romania)"
msgstr ""

#: Language.class:151
msgid "Russian (Russia)"
msgstr "Russian (Rusia)"

#: Language.class:154
msgid "Slovenian (Slovenia)"
msgstr "Slovenia (Slovenia)"

#: Language.class:157
msgid "Albanian (Albania)"
msgstr ""

#: Language.class:160
msgid "Serbian (Serbia & Montenegro)"
msgstr ""

#: Language.class:163
msgid "Swedish (Sweden)"
msgstr "Swedish (Sweden)"

#: Language.class:166
msgid "Turkish (Turkey)"
msgstr "Turkish (Turkey)"

#: Language.class:169
msgid "Ukrainian (Ukrain)"
msgstr ""

#: Language.class:172
msgid "Vietnamese (Vietnam)"
msgstr ""

#: Language.class:175
msgid "Wallon (Belgium)"
msgstr "Wallon (Belgia)"

#: Language.class:178
msgid "Simplified chinese (China)"
msgstr "Cina"

#: Language.class:179
msgid "Traditional chinese (Taiwan)"
msgstr "Taiwan"

#: Language.class:241
msgid "Unknown"
msgstr "Tidak dikenal"
